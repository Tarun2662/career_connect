from django.shortcuts import render,redirect,get_object_or_404,get_list_or_404
from django.contrib.auth.models import User
from . models import profile,company,applied
from django.contrib.auth import authenticate ,login,logout
from django.core.mail import send_mail
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.db import IntegrityError 
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.hashers import make_password
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.decorators import  permission_required,login_required
from django.db.models import Q
from .forms import ImageForm
from django.contrib import messages
from geopy.geocoders import Nominatim
from geopy.distance import geodesic
import random
import math

import re

def login_user(request):
    if request.method=='GET':
        return render(request,'board/login_page.html')
    if request.method=='POST':
        input_user=request.POST.get('username','')
        input_password=request.POST.get('password','')
        user=authenticate(username=input_user,password=input_password)
        if user is not None:
            login(request,user)
            u=get_object_or_404(User,username=request.user)
            if u.last_name=='company':
               return redirect('homecompany')
            else:
                return redirect('profile_details')
        else:
            context={'error_message':'invalid user name or password'}
            return render(request,'board/login_page.html',context)
class verify:
    OTP=1
    firstname=None
    lastname='company'
    email=None
    password=None
    valid=None

    def send_otp(email_):
        digits = "0123456789"
        OTP = ""
        for i in range(4) :
            OTP += digits[math.floor(random.random() * 10)]
        
        send_mail(
            "OTP FOR AUTHENTICATION",
            OTP +"is your otp",
            "settings.EMAIL_HOST_USER",
            [email_],
            fail_silently=False,

        )
        return OTP
   
    def emp_register(request):
        if request.method=='GET':
            return render(request,'board/empregister_page.html')
        if request.method=='POST':
            if 'register' in request.POST:
                verify.firstname=request.POST.get('company_name','')
                
                verify.email=request.POST.get('email','')
                verify.password=request.POST.get('password','')
                regular_expression = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]{6,20}$"
                pattern = re.compile(regular_expression)
                verify.valid = re.search(pattern, verify.password)
               
                
                try:
                    check=User.objects.get(username=verify.email)
                    context={'error':'ALREADY REGISTERED PLEASE LOGIN'}
                    return render(request,'board/empregister_page.html',context)
                except  ObjectDoesNotExist:
                    if verify.valid:
                        verify.OTP=verify.send_otp(verify.email)
                        context={'sent':'OTP sent sucessfully'}
                        return render(request,'board/upload.html',context)
                    else:
                        context={'error':'weak password'}
                        return render(request,'board/empregister_page.html',context)
            if'sub' in request.POST:  
                input_otp=request.POST.get('otp')
                message=f''' HI {verify.firstname}, 
                Hope you are doing well. 
                Thankyou for choosing career connect .
                You have successfully Registered!
                Regards
                career connect Team'''
                if verify.OTP==input_otp:
                   
                    
                    from django.contrib.auth.hashers import make_password
                    hashed_password = make_password(verify.password)
                    user=User.objects.create(first_name=verify.firstname,last_name=verify.lastname,email=verify.email,username=verify.email,password=hashed_password)
                    user.save()
            
                    send_mail(
                    "sucessfully registered",
                    message,
                    "settings.EMAIL_HOST_USER",
                    [verify.email],
                    fail_silently=False,
        
                    )
                    return redirect("homecompany")
                else:

                    context={'error':'INVALID OTP'}
                    return render(request,'board/upload.html',context)
            
    def register_user(request):
        if request.method=='GET':
            return render(request,'board/register_page.html')
        if request.method=='POST':
            if 'register' in request.POST:
                verify.firstname=request.POST.get('first_name','')
                verify.lastname=request.POST.get('last_name','')
                verify.email=request.POST.get('email','')
                verify.password=request.POST.get('password','')
                regular_expression = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]{6,20}$"
                pattern = re.compile(regular_expression)
                verify.valid = re.search(pattern, verify.password)
               
                
                try:
                    check=User.objects.get(username=verify.email)
                    context={'error':'ALREADY REGISTERED PLEASE LOGIN'}
                    return render(request,'board/register_page.html',context)
                except  ObjectDoesNotExist:
                    if verify.valid:
                        verify.OTP=verify.send_otp(verify.email)
                        context={'sent':'OTP sent sucessfully'}
                        return render(request,'board/upload.html',context)
                    else:
                        context={'error':'weak password'}
                        return render(request,'board/register_page.html',context)
                    
                  
            
            if'sub' in request.POST:  
                input_otp=request.POST.get('otp')
                message=f''' HI {verify.firstname}, 
                Hope you are doing well. 
                Thankyou for choosing career connect .
                You have successfully Registered!
                Regards
                career connect Team'''
                if verify.OTP==input_otp:
                   
                    
                    from django.contrib.auth.hashers import make_password
                    hashed_password = make_password(verify.password)
                    user=User.objects.create(first_name=verify.firstname,last_name=verify.lastname,email=verify.email,username=verify.email,password=hashed_password)
                    user.save()
            
                    send_mail(
                    "sucessfully registered",
                    message,
                    "settings.EMAIL_HOST_USER",
                    [verify.email],
                    fail_silently=False,
        
                    )
                    return redirect("profile_details")
                else:

                    context={'error':'INVALID OTP'}
                    return render(request,'board/upload.html',context)
    
            
        
@login_required(login_url='login_user')
def profile_details(request):
    try:
        check=profile.objects.get(username=request.user)
        if check is not None:
            return redirect('show_profile')
    except ObjectDoesNotExist:
        if request.method=='GET':
            form=ImageForm()
            return render(request,'board/newprofile.html',{'form':form})
        if request.method=='POST':
            form = ImageForm(request.POST, request.FILES)
            if form.is_valid():
                img = form.cleaned_data.get("profile_photo")
                mobile_=request.POST.get('mobile_number')
                location_=request.POST.get('location')
                experience_=request.POST.get('experience')
                availability_=request.POST.get('availability')
                gender_=request.POST.get('gender')
                resume_=request.POST.get('resume')
                graduation_=request.POST.get('stream')
                grad_clg_name_=request.POST.get('grad_clg_name')
                grad_per_=request.POST.get('grad_per')
                grad_pass_year_=request.POST.get('grad_pass_year')
                higher_education=request.POST.get('high_stream')
                high_clg_name_=request.POST.get('high_clg_name')
                high_per_=request.POST.get('high_per')
                high_pass_year_=request.POST.get('high_pass_year')
                scl_ed_=request.POST.get('scl_ed')
                scl_name_=request.POST.get('scl_name')
                scl_pass_year_=request.POST.get('scl_pass_year')
                scl_per_=request.POST.get('scl_per')
                skill_=request.POST.get('skill')
                user=get_object_or_404(User,username=request.user)
                x=user.first_name + user.last_name
            
            
        
        
        
                pro=profile(username=request.user,name=x,profile_photo=img,gender=gender_,mobile_number=mobile_ ,location=location_,experience=experience_,
                                    availability=availability_,resume=resume_,graduation=graduation_,grad_clg_name=grad_clg_name_,grad_pass_year=grad_pass_year_,
                                   grad_per=grad_per_,higher_ed=higher_education,higher_ed_clg_name=high_clg_name_,highereducation_passout_year=high_pass_year_,
                                   higher_ed_per=high_per_,sec_ed=scl_ed_,sec_ed_scl_name=scl_name_,sec_ed_pass_year=scl_pass_year_,sec_ed_percentage=scl_per_,skill=skill_)
        

                pro.save() 
              
                return redirect('show_profile')
        
def show_profile(request):
    
    name=User.objects.get(username=request.user)
    pro=profile.objects.get(username=request.user)
    l=get_list_or_404(company)
    im=get_object_or_404(profile,username=request.user)
    
    context={'p':pro,'name':name ,'l':l,'li':im}
    return render(request,'board/homepage.html',context)
def logout_(request):
    logout(request)
    return redirect('login_user')
def company_(request):
    if request.method=='GET':
        return render(request,'board/homecompany.html')
    if request.method=='POST':
        
        name_=get_object_or_404(User,username=request.user)
        job_role_=request.POST.get('job_role')
        qualification_=request.POST.get('qualification')
        batch_=request.POST.get('batch')
        experience_=request.POST.get('experience')
        location_=request.POST.get('job_location')
        jobcode=request.POST.get('job_code')
        skills_=request.POST.get('skills')
        lastdate_=request.POST.get('lastdate')
        ctc_=request.POST.get('ctc')
        obj=User.objects.get(username=request.user)

        com=company(name=name_.first_name,job_role=job_role_,job_code=jobcode,skills=skills_,lastdate=lastdate_,ctc=ctc_,qualification=qualification_,batch=batch_,experience=experience_,job_location=location_,companymail=request.user,user=obj)
        com.save()
        return redirect('companyposts')
def update(request,id_):
    u=get_object_or_404(profile,pk=id_)
    context={'u':u}
    if request.method=='GET':
        return render(request,'board/update.html',context)
    if request.method=='POST':
         profile_photo_=request.POST.get('profile_photo')
         mobile_=request.POST.get('mobile_number')
         location_=request.POST.get('location')
         experience_=request.POST.get('experience')
         availability_=request.POST.get('availability')
         gender_=request.POST.get('gender')
         resume_=request.POST.get('resume')
         graduation_=request.POST.get('stream')
         grad_clg_name_=request.POST.get('grad_clg_name')
         grad_per_=request.POST.get('grad_per')
         grad_pass_year_=request.POST.get('grad_pass_year')
         higher_education=request.POST.get('high_stream')
         high_clg_name_=request.POST.get('high_clg_name')
         high_per_=request.POST.get('high_per')
         high_pass_year_=request.POST.get('high_pass_year')
         scl_ed_=request.POST.get('scl_ed')
         scl_name_=request.POST.get('scl_name')
         scl_pass_year_=request.POST.get('scl_pass_year')
         scl_per_=request.POST.get('scl_per')
         skill_=request.POST.get('skill')
         u.username=request.user
         u.profile_photo=profile_photo_
         u.gender=gender_
         u.mobile_number=mobile_ 
         u.location=location_
         u.experience=experience_
         u.availability=availability_
         u.resume=resume_
         u.graduation=graduation_
         u.grad_clg_name=grad_clg_name_
         u.grad_pass_year=grad_pass_year_
         u.grad_per=grad_per_
         u.higher_ed=higher_education
         u.higher_ed_clg_name=high_clg_name_
         u.highereducation_passout_year=high_pass_year_
         u.higher_ed_per=high_per_
         u.sec_ed=scl_ed_
         u.sec_ed_scl_name=scl_name_
         u.sec_ed_pass_year=scl_pass_year_
         u.sec_ed_percentage=scl_per_
         u.skill=skill_
         u.save()
         return redirect('show_profile')


def homecompany(request):
    posts=company.objects.filter(companymail=request.user)
    n=posts.count()
    ap=applied.objects.filter(apliedcompany=request.user)
    name=get_object_or_404(User,username=request.user)
    return render(request,'board/homecompany.html',{'name':name,'n':n,'m':ap.count()})
@login_required(login_url='login_user')   
def homepage(request):
    return render(request,'board/homepage.html')
def search(request):
    if request.method=='POST':
        s=request.POST.get('search')
        name=User.objects.get(username=request.user)
        pro=profile.objects.get(username=request.user)
        l=get_list_or_404(company)
        im=get_object_or_404(profile,username=request.user)
        geolocator = Nominatim(user_agent="MyApp")
        try:
            location = geolocator.geocode(s)
            origin=(location.latitude ,location.longitude)
            ls = [] #nearby_companies = []
       
            loc=get_list_or_404(company)
            #companies = get_list_or_404(company)
            address=[]
            for i in loc: #for company in companies
                location = geolocator.geocode(i.job_location)
                dist=(location.latitude,location.longitude)
                distance=geodesic(origin, dist).kilometers
            
                if distance<=100:
                    address.append(i.job_location)
                    #ls.append(i)
            ls=company.objects.filter(job_location__in = address)
            
        except UnboundLocalError:
            ls=None
        except AttributeError:
            ls=None
        context={'p':pro,'name':name ,'l':ls,'li':im,}
        return render(request,'board/homepage.html',context)
def companyposts(request):
    posts=company.objects.filter(companymail=request.user)
    n=posts.count()
    name=get_object_or_404(User,username=request.user)
    ap=applied.objects.filter(apliedcompany=request.user)
    context={'post':posts,'name':name,'n':n,'m':ap.count()}
    return render(request,'board/companyposts.html',context)
def postdelete(request,id_):
    d=get_object_or_404(company,pk=id_)
    d.delete()
    return redirect('companyposts')
def jobdescription(request):
    return render(request,'board/jobdescription.html')
def application(request,id_):
    app=get_object_or_404(company,pk=id_)
    per=get_object_or_404(profile,username=request.user)
    try:
        obj=applied.objects.create(jobcode=app.job_code,companys=app,apliedcompany=app.companymail,person=per,status='applied')
        obj.save()
        message='SUCESSFULLY APPLIED'
    except IntegrityError:
        message='ALREADY APPLIED'
       
    job=get_list_or_404(applied)
    name=User.objects.get(username=request.user)
    pro=profile.objects.get(username=request.user)
    l=get_list_or_404(company)
    im=get_object_or_404(profile,username=request.user)
    messages.success(request, "APPLIED" )
    
    
    context={'p':pro,'name':name ,'l':l,'li':im,'job':job,'flag':message}
    return render(request,'board/homepage.html',context)

def com_application(request):
    ap=applied.objects.filter(Q(apliedcompany=request.user)&Q(status='applied'))
    posts=company.objects.filter(companymail=request.user)
    name=get_object_or_404(User,username=request.user)
    context={'ap':ap,'n':ap.count(),'m':posts.count(),'name':name}
    return render(request,'board/application.html',context)
def view(request,id_):
    ap=applied.objects.filter(Q(apliedcompany=request.user)&Q(status='applied') )
    pro=profile.objects.get(pk=id_)
    context={'ap':ap,'n':ap.count(),'pro':pro}
    return render(request,'board/application.html',context)
def accept(request,id_,code):
    per=profile.objects.get(pk=id_)
    com=company.objects.get(job_code=code)
    accepted=applied.objects.get(Q(companys=com)&Q(person=per))
    accepted.status='accept'
    accepted.save()
    message=f''' HI {accepted.person.name} 
    GREETINGS! FROM CAREER CONNECT 
    you are shortlisted .
    we will get back to you
    regards career connect'''
    send_mail(
            "PROFILE SHORTLISTED",
            message,
            "settings.EMAIL_HOST_USER",
            [accepted.person.username],
            fail_silently=False,
        
            )
    return redirect('com_application')
def reject(request,id_,code):
    per=profile.objects.get(pk=id_)
    com=company.objects.get(job_code=code)
    re=applied.objects.get(Q(companys=com)&Q(person=per))
    re.status='reject'
    re.save()
    return redirect('com_application')
def post_view(request,id_):
    v=company.objects.get(pk=id_)
    name=User.objects.get(username=request.user)
    pro=profile.objects.get(username=request.user)
    l=get_list_or_404(company)
    im=get_object_or_404(profile,username=request.user)
    context={'p':pro,'name':name ,'l':l,'li':im,'v':v}
    return render(request,'board/homepage.html',context)


def empregister_page(request):
    return render(request,'board/empregister_page.html')
def complete_profile(request):
    p=profile.objects.get(username=request.user)
    t=User.objects.get(username=request.user)
    return render(request,'board/completeprofile.html',{'p':p,'t':t})

def report_footer(request):
    return render(request,'board/report_footer.html')
def career_footer(request):
    return render(request,'board/career_footer.html')
def aboutus_footer(request):
    return render(request,'board/aboutus_footer.html')
def fraud_footer(request):
    return render(request,'board/fraud_footer.html')   
def trust_footer(request):
    return render(request,'board/trust_footer.html')
def terms_footer(request):
    return render(request,'board/terms_footer.html')
def help_footer(request):
    return render(request,'board/help_footer.html')
def notice_footer(request):
    return render(request,'board/notice_footer.html')
def grievance_footer(request):
    return render(request,'board/grievance_footer.html')
def emphome_footer(request):
    return render(request,'board/emphome_footer.html')
def site_footer(request):
    return render(request, 'board/site_footer.html')
def privacy_policy(request):
    return render(request, 'board/privacy_policy.html')
def accept_list(request):
    ap=applied.objects.filter(Q(apliedcompany=request.user)&Q(status='applied'))
    acpt=applied.objects.filter(Q(apliedcompany=request.user)&Q(status='accept'))
    context={'ap':ap,'n':ap.count(),'acpt':acpt}
    return render(request,'board/application.html',context)
def reject_list(request):
    ap=applied.objects.filter(Q(apliedcompany=request.user)&Q(status='applied'))
    rjct=applied.objects.filter(Q(apliedcompany=request.user)&Q(status='reject'))
    context={'ap':ap,'n':ap.count(),'rjct':rjct}
    return render(request,'board/application.html',context)

    
        





    




    


    




