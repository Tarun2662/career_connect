from django.db import models

from django.contrib.auth.models import User
class profile(models.Model):
    username=models.CharField(max_length=200 ,null=True)
    name=models.CharField(max_length=100,null=True)
    profile_photo=models.ImageField(upload_to='images/',null=True)
    gender=models.CharField(max_length=200 ,null=True)
    mobile_number=models.CharField(max_length=12)
    location=models.CharField(max_length=250)
    experience=models.CharField(max_length=250)
    availability=models.DateField()
    resume=models.FileField(upload_to='files/' ,null=True)
    graduation=models.CharField(max_length=50 ,null=True) 
    grad_clg_name=models.CharField(max_length=200,null=True)
    grad_pass_year=models.CharField(max_length=4,null=True)
    grad_per=models.CharField(max_length=4,null=True)
    higher_ed=models.CharField(max_length=50,null=True)
    higher_ed_clg_name=models.CharField(max_length=200,null=True)
    highereducation_passout_year=models.CharField(max_length=4,null=True)
    higher_ed_per=models.CharField(max_length=4,null=True)
    sec_ed=models.CharField(max_length=50,null=True)
    sec_ed_scl_name=models.CharField(max_length=200,null=True)
    sec_ed_pass_year=models.CharField(max_length=4,null=True)
    sec_ed_percentage=models.CharField(max_length=4,null=True)
    skill=models.TextField(null=True)
    user=models.ForeignKey(User,on_delete=models.CASCADE,null=True)
    
class company(models.Model):
    name=models.CharField(max_length=100,null=True)
    job_role=models.CharField(max_length=200 ,null=True)
    job_code=models.CharField(max_length=10,null=True)
    skills=models.CharField(max_length=100,null=True)
    lastdate=models.DateField(null=True)
    ctc=models.CharField(max_length=20,null=True)
    qualification=models.CharField(max_length=50, null=True)
    batch=models.CharField(max_length=20,null=True)
    experience=models.IntegerField(null=True)
    job_location=models.CharField(max_length=100,null=True)
    companymail=models.CharField(max_length=200,null=True)
    user=models.ForeignKey(User,on_delete=models.CASCADE,null=True)


class applied(models.Model):
    jobcode=models.CharField(max_length=10,unique=True,null=True)
    companys=models.ForeignKey(company,on_delete=models.CASCADE,null=True)
    apliedcompany=models.CharField(max_length=100)
    person=models.ForeignKey(profile,on_delete=models.CASCADE ,null=True)
    status=models.CharField(max_length=10,null=True)
    
    

    


    
    






