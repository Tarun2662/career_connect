# Generated by Django 4.2.1 on 2023-05-26 16:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('board', '0004_company'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='batch',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='companymail',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='experience',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='job_location',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='job_role',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='qualification',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
