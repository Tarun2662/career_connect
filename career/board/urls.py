from django.urls import path
from . import views
from django.conf.urls.static import static  # new
from django.conf import settings 
from django.contrib.staticfiles.urls import staticfiles_urlpatterns 
urlpatterns=[path('login',views.login_user,name='login_user'),
             path('register',views.verify.register_user,name='register_user'),
             path('profile',views.profile_details,name='profile_details'),
             path('empregister',views.verify.emp_register,name='empregister'),
             path('show',views.show_profile,name='show_profile'),
             path('company',views.company_,name='company'),
             path('logout',views.logout_,name='logout_'),
             path('update/<int:id_>',views.update,name='update_'),
             path('homepage',views.homepage,name='homepage'),
             path('homecompany',views.homecompany,name='homecompany'),
             path('search',views.search,name="search"),
             path('view/<int:id_>',views.view,name="view"),
             path('jobdescription',views.jobdescription,name='jobdescription'),
             path('companyposts',views.companyposts,name='companyposts'),
             path('postdelete/<int:id_>',views.postdelete,name="postdelete"),
             path('application',views.com_application,name='com_application'),
             path('accept/<int:id_>/<int:code>',views.accept,name="accept"),
             path('reject/<int:id_>/<int:code>',views.reject,name="reject"),
             path('apply/<int:id_>',views.application,name='apply'),
             path('postview/<int:id_>',views.post_view,name="postview"),
             path('empregister_page',views.empregister_page,name='empregister_page'),
             path('completeprofile',views.complete_profile,name='completeprofile'),
             path('privacy_policy',views.privacy_policy,name='privacy_policy'),
             path('report_footer',views.report_footer,name='report_footer'),
             path('career_footer',views.career_footer,name='career_footer'),
             path('fraud_footer',views.fraud_footer,name='fraud_footer'),
             path('aboutus_footer',views.aboutus_footer,name='aboutus_footer'),
             path('trust_footer',views.trust_footer,name='trust_footer'),
             path('terms_footer',views.terms_footer,name='terms_footer'),
             path('help_footer',views.help_footer,name='help_footer'),
             path('notice_footer',views.notice_footer,name='notice_footer'),
             path('site_footer',views.site_footer,name='site_footer'),
             path('emphome_footer',views.emphome_footer,name='emphome_footer'),
             path('grievance_footer',views.grievance_footer,name='grievance_footer'),
             path('accepted',views.accept_list,name='accept_list'),
             path('rejected',views.reject_list,name='reject_list')]
if settings.DEBUG:  # new
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()