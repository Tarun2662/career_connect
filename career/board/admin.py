from django.contrib import admin
from .models import profile,company,applied
from django.contrib.auth.models import User
# Register your models here.
admin.site.register(profile)
admin.site.register(company)
admin.site.register(applied)